import React, { Component } from 'react';
import "../css/Formulaire.css"
class FooterMain extends Component {
    render() {
        return (
            <div>
                <div className="row center">
                    <img src={require("../images/moneo.png")} />
                    <img style={{ padding: 15 }} src={require("../images/route.png")} />
                    <img style={{ padding: 15 }} src={require("../images/lefort_francheteau.png")} />
                    <img style={{ padding: 15 }} src={require("../images/edf.png")} />
                    <img style={{ padding: 15 }} src={require("../images/Eiffage.png")} />
                    <img style={{ padding: 15 }} src={require("../images/Benchmark.png")} />
                </div>
                <div className="row center" style={{ height: 80, backgroundColor: "#171717" }}>
                    <div className="col-xs-12 col-md-5">
                        <img style={{ padding: 15 }} src={require("../images/facebook.png")} />
                        <img style={{ padding: 15 }} src={require("../images/twitter.png")} />
                        <img style={{ padding: 15 }} src={require("../images/linkedin.png")} />
                        <img style={{ padding: 15 }} src={require("../images/pinterest.png")} />
                        <img style={{ padding: 15 }} src={require("../images/vimeo.png")} />
                    </div>
                    <div className="col-xs-12 col-md-6" style={{ padding: 15, color: "#676767", fontSize: 14, backgroundColor: "#171717" }}>© Copyrights JS-attitude 2019. All rights reserved. - Conditions générales</div>
                    <div className="col-xs-12 col-md-1"><div className="imgScroll" onClick={() => window.scrollTo({ top: 0 })}> <img style={{marginTop:16,width:16,height:16}} src={require("../images/up-arrow.png")}/></div></div>
                </div>
            </div >

        )
    }
}

export default FooterMain;