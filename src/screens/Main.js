import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import PlacesAutocomplete from 'react-places-autocomplete';
import Calendar from 'react-calendar';
import InputText from '../components/InputText';
import Header from '../components/Header';
import Footer from '../components/Footer';
import "../css/Formulaire.css"
import "../css/DropDownMenu.css"
import "../css/InputRange.css"
import '../css/Calendar.css';
import "../css/Body.css"

import firebase from '../db/firebase';

//Type of trajet
const datas_type_Trajet = ["Aller simple", "Aller-Retour", "Circuit sur plusieurs heures", "Circuit sur plusieurs jours"];
//Type of Vehicules
const datas_type_Vehicule = ["Autocar 53-80 places", "Berline 3 places", "Minivan 6-8 places", "Minibus 17 places"];

//Ville de france
const searchOptions = {
    componentRestrictions: {
        country: ['fr']
    },
    types: ['(cities)']
}

//Adresse 
const searchOptionsAdress = {
    componentRestrictions: {
        country: ['fr']
    },
    types: ['address']
}

const styleError = { borderWidth: 1, borderStyle: "Solid", borderColor: "#4799df", boxShadow: 3 }
const styleValid = { borderWidth: 1, borderStyle: "Solid", borderColor: "#c8c8c8", boxShadow: 3 }

class Main extends Component {
    //#region Constructeurs  
    constructor() {
        super()
        // Initialise les propriétés du composant
        this.state = {
            dataTable: [{ key: '', designation: '', quantite: '' }, { key: '', designation: '', quantite: '' }, { key: '', designation: '', quantite: '' }, { key: '', designation: '', quantite: '' }, { key: '', designation: '', quantite: '' }],
            showNext: false,
            displayMenuTypeTrajet: false,
            displayMenuTypeVehicule: false,
            isShowDateDepart: false,
            isShowDateFin: false,
            typeTrajet: "",
            typeVehicule: "",
            villeDepart: "",
            villeArrive: "",
            adresseRamassage: '',
            adresseDestination: '',
            addressEtapes: '',
            date: "",
            dateFin: '',
            dateDepart: "",
            dateFinDeService: '',
            nbr_vehicule: 0,
            nbr_passenger: 0,
            nbrHeure: 0,
            nbrJour: 0,
            prixTotal: "$0.00",
            distance: '',
            input_name: "",
            input_email: "",
            input_adresse: "",
            input_phone: "",
            input_entreprise: "",
            infoComplementaire: "",
            heureDebutService: "",
            heureFinService: "",
            styleInputTypeTrajet: null,
            styleInputTypeVehicule: null,
            styleInputVilleDepart: null,
            styleInputVilleArrive: null,
            styleInputName: null,
            styleInputEmail: null,
            styleInputPhone: null,
            listEtapes: [],
            prixUnitaire: '',
            montant: '',
            isAutocar: false,
            isMinibus: false,
            isMinivan: false,
            isBerline: false,
            isFinish: false,
            styleImgTypeVehicule: null,
            styleVehiculeAutocar: {
                width: 100,
                height: 50
            },
            styleVehiculeMinibus: {
                width: 100,
                height: 50
            },
            styleVehiculeMinivan: {
                width: 100,
                height: 50
            },
            styleVehiculeBerline: {
                width: 100,
                height: 50
            }
        }
        this.id_user = ''
    }
    //#endregion Constructeurs  


    //#region Type Trajet
    // Méthode permettant d'afficher le Dropdown TypeTrajet
    showDropdownTypeTrajet = (event) => {
        event.preventDefault();
        this.setState({ displayMenuTypeTrajet: true }, () => {
            document.addEventListener('click', this.hideDropdownTypeTrajet);
        });
    }
    hideDropdownTypeTrajet = () => {
        this.setState({ displayMenuTypeTrajet: false }, () => {
            document.removeEventListener('click', this.hideDropdownTypeTrajet);
        });
    }
    onClickHandlerTypeTrajet = (type) => {
        let style = { borderWidth: 1, borderStyle: "Solid", borderColor: "#fb17ce", boxShadow: 3 }
        if (type !== "") {
            style = { borderWidth: 1, borderStyle: "Solid", borderColor: "#c8c8c8", boxShadow: 3 }
        }
        this.setState({
            typeTrajet: type,
            styleInputTypeTrajet: style,
            nbrHeure: 0,
            nbrJour: 0
        })
    }
    //#endregion


    //#region Type Vehicule
    // Méthode permettant d'afficher le Dropdown TypeVehicule
    showDropdownTypeVehicule = (event) => {
        event.preventDefault();
        this.setState({ displayMenuTypeVehicule: true }, () => {
            document.addEventListener('click', this.hideDropdownTypeVehicule);
        });
    }

    hideDropdownTypeVehicule = () => {
        this.setState({ displayMenuTypeVehicule: false }, () => {
            document.removeEventListener('click', this.hideDropdownTypeVehicule);
        });
    }
    onClickHandlerTypeVehicule = (type) => {
        let style = { borderWidth: 1, borderStyle: "Solid", borderColor: "#fb17ce", boxShadow: 3 }
        if (type !== "") {
            style = { borderWidth: 1, borderStyle: "Solid", borderColor: "#c8c8c8", boxShadow: 3 }
        }

        let styleImgWithborder = { width: 100, height: 50, borderWidth: 1, borderStyle: "Solid", borderColor: "#4799df" }
        let styleImgWithoutborder = { width: 100, height: 50}
        
        if (type === "Autocar 53-80 places") {
            this.setState({
                typeVehicule: type,
                styleInputTypeVehicule: style,
                styleVehiculeAutocar: styleImgWithborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder
            })
        } else if (type === "Minibus 17 places") {
            this.setState({
                typeVehicule: type,
                styleInputTypeVehicule: style,
                styleVehiculeMinibus: styleImgWithborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder

            })
        } else if (type === "Minivan 6-8 places") {
            this.setState({
                typeVehicule: type,
                styleInputTypeVehicule: style,
                styleVehiculeMinivan: styleImgWithborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder
            })
        } else {
            this.setState({
                typeVehicule: type,
                styleInputTypeVehicule: style,
                styleVehiculeBerline: styleImgWithborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder
            })
        }
    }
    //#endregion


    //#Begin Adress Départ
    _renderAutoCompleteDepart = () => {
        return (
            <PlacesAutocomplete
                value={this.state.villeDepart}
                onChange={this.handleChangeVilleDepart}
                onSelect={this.handleSelectVilleDepart}
                searchOptions={searchOptions}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4 subTitle">Ville de départ</div>
                        <div className="col-xs-12 col-md-8">
                            <input
                                style={this.state.styleInputVilleDepart}
                                {...getInputProps({ placeholder: 'Entrer la ville de départ', className: 'location-search-input inputzone' })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active ? { backgroundColor: '#fafafa', cursor: 'pointer' } : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>)}
            </PlacesAutocomplete>
        )
    }
    //Change address depart
    handleChangeVilleDepart = address => {
        let style = styleError;
        if (address !== "") { style = styleValid }
        this.setState({ villeDepart: address, styleInputVilleDepart: style });

    };
    //Select Address Depart
    handleSelectVilleDepart = address => {
        this.setState({ villeDepart: address, styleInputVilleDepart: styleValid });
    };
    //#endregion


    //#Begin Adress Arrivée
    _renderAutoCompleteArrivee = () => {
        return (
            <PlacesAutocomplete
                value={this.state.villeArrive}
                onChange={this.handleChangeVilleArrive}
                onSelect={this.handleSelectVilleArrive}
                searchOptions={searchOptions}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4 subTitle">Ville d’arrivée</div>
                        <div className="col-xs-12 col-md-8">
                            <input
                                style={this.state.styleInputVilleArrive}
                                {...getInputProps({ placeholder: 'Entrer la ville arrivée', className: 'location-search-input inputzone' })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active ? { backgroundColor: '#fafafa', cursor: 'pointer' } : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        )
    }

    //Change address Arrivee
    handleChangeVilleArrive = address => {
        let style = styleError;
        if (address !== "") { style = styleValid }
        this.setState({ villeArrive: address, styleInputVilleArrive: style });
    };

    //Select address arrivee
    handleSelectVilleArrive = address => {
        this.setState({ villeArrive: address, styleInputVilleArrive: styleValid });
    };
    //#endregion


    //#Begin Adress Ramassage
    _renderAutoCompleteRamassage = () => {
        return (
            <PlacesAutocomplete
                value={this.state.adresseRamassage}
                onChange={this.handleChangeAdrRamassage}
                onSelect={this.handleSelectAdrRamassage}
                searchOptions={searchOptionsAdress}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4 subTitle">Adresse de ramassage</div>
                        <div className="col-xs-12 col-md-8">
                            <input
                                {...getInputProps({ placeholder: 'Entrer adresse de ramassage', className: 'location-search-input inputzone', })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active ? { backgroundColor: '#fafafa', cursor: 'pointer' } : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        )
    }

    //Change address Ramassage
    handleChangeAdrRamassage = address => {
        this.setState({ adresseRamassage: address });
    };

    //Select address Ramassage
    handleSelectAdrRamassage = address => {
        this.setState({ adresseRamassage: address });
    };
    //#endregion


    //#Begin Adress Destination
    _renderAutoCompleteDestination = () => {
        return (
            <PlacesAutocomplete
                value={this.state.adresseDestination}
                onChange={this.handleChangeAdrDestination}
                onSelect={this.handleSelectAdrDestination}
                searchOptions={searchOptionsAdress}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4 subTitle">Adresse de destination</div>
                        <div className="col-xs-12 col-md-8">
                            <input
                                {...getInputProps({ placeholder: 'Entrer adresse de destination', className: 'location-search-input inputzone', })} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active ? { backgroundColor: '#fafafa', cursor: 'pointer' } : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        )
    }
    //Change address Destination
    handleChangeAdrDestination = address => {
        this.setState({ adresseDestination: address });
    };
    //Select address Destination
    handleSelectAdrDestination = address => {
        this.setState({ adresseDestination: address });
    };
    //#endregion



    //#Begin Adresse des étapes
    handleChangeAdr = i => address => {
        let listEtapes = [...this.state.listEtapes]
        listEtapes[i] = address
        this.setState({ listEtapes })
    };
    handleSelectAdr = i => address => {
        let listEtapes = [...this.state.listEtapes]
        listEtapes[i] = address
        this.setState({ listEtapes })
    };
    handleDeleteStep = i => e => {
        e.preventDefault()
        let listEtapes = [...this.state.listEtapes.slice(0, i), ...this.state.listEtapes.slice(i + 1)]
        this.setState({ listEtapes })
    }
    addStep = () => {
        let listEtapes = this.state.listEtapes.concat([''])
        this.setState({ listEtapes })
    }
    //Ajouter une étape
    _renderAutoCompleteStep = () => {
        return (this.state.listEtapes.map((step, index) => (
            <PlacesAutocomplete
                onChange={this.handleChangeAdr(index)}
                onSelect={this.handleSelectAdr(index)}
                value={step}
                searchOptions={searchOptionsAdress}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4"></div>
                        <div className="col-xs-12 col-md-8">
                            <div
                                style={{
                                    position: 'relative',
                                    marginBottom: 10
                                }}>
                                <input
                                    {...getInputProps({ placeholder: 'Entrer l\'adresse de l\'étape ...', className: 'location-search-input inputzone', })} />
                                <img src={require("../images/cancel.png")} style={{ width: 24, height: 24, position: 'absolute', right: 5, top: 12 }} onClick={this.handleDeleteStep(index)} />
                            </div>
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                        ? 'suggestion-item--active'
                                        : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                        ? {
                                            backgroundColor: '#fafafa',
                                            cursor: 'pointer'
                                        }
                                        : {
                                            backgroundColor: '#ffffff',
                                            cursor: 'pointer'
                                        };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        )))
    }
    _renderAutoCompleteAdrEtapes = () => {
        return (
            <PlacesAutocomplete
                value={this.state.addressEtapes}
                onChange={this.handleChangeAdrEtapes}
                onSelect={this.handleSelectAdrEtapes}
                searchOptions={searchOptionsAdress}>
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className="row">
                        <div className="col-xs-12 col-md-4 subTitle">Adresse des étapes</div>
                        <div className="col-xs-12 col-md-8">
                            <input
                                {...getInputProps({ placeholder: 'Entrer adresse de étape', className: 'location-search-input inputzone', })} />
                            <img src={require("../images/add.png")} style={{ width: 24, height: 24, position: 'absolute', right: 20, top: 12 }} onClick={() => this.addStep()} />
                            <div className="autocomplete-dropdown-container">
                                {loading && <div>Loading...</div>}
                                {suggestions.map(suggestion => {
                                    const className = suggestion.active ? 'suggestion-item--active' : 'suggestion-item';
                                    // inline style for demonstration purpose
                                    const style = suggestion.active ? { backgroundColor: '#fafafa', cursor: 'pointer' } : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                    return (
                                        <div {...getSuggestionItemProps(suggestion, { className, style, })}>
                                            <span>{suggestion.description}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
        )
    }

    //Change address Etapes
    handleChangeAdrEtapes = address => {
        this.setState({ addressEtapes: address });
    };

    //Select address Etapes
    handleSelectAdrEtapes = address => {
        this.setState({ addressEtapes: address });
    };
    //#endregion


    //#Begin date Depart
    // fonction qui permet de mettre à jour le state date
    onChange = (date) => {
        var date1 = new Date(date),
            mnth = ("0" + (date1.getMonth() + 1)).slice(-2),
            day = ("0" + date1.getDate()).slice(-2);
        let result = [day, mnth, date1.getFullYear(),].join("/");
        this.setState({ dateDepart: result, date: date, isShowDateDepart: false })

    }
    //fonction qui permet d'afficher le calendrier
    showDateDepart = (event) => {
        this.setState({ isShowDateDepart: true })
    }
    //#endregion


    //#Begin date Fin
    // fonction qui permet de mettre à jour le state date
    onChangeDateFin = (date) => {
        var date1 = new Date(date),
            mnth = ("0" + (date1.getMonth() + 1)).slice(-2),
            day = ("0" + date1.getDate()).slice(-2);
        let result = [day, mnth, date1.getFullYear(),].join("/");
        this.setState({ dateFinDeService: result, dateFin: date, isShowDateFin: false })

    }
    //fonction qui permet d'afficher le calendrier
    showDateFin = (event) => {
        this.setState({ isShowDateFin: true })
    }
    //#endregion


    //#region Nombre Véhicule
    //Change le nombre des véhicules
    handleChangeNbrVehicle = (event) => {
        this.setState({ nbr_vehicule: event.target.value });
    }
    //#endregion


    //#region Nombre Passager
    //Change le nombre des passagers
    handleChangeNbrPassenger = (event) => {
        this.setState({ nbr_passenger: event.target.value });
    }
    //#endregion


    //#region calculate price
    //End adress step Fonction qui permet de calculer distance entre deux points
    getDistance = async (index, id_user) => {
        let service = new window.google.maps.DistanceMatrixService();
        service.getDistanceMatrix({
            origins: [this.state.villeDepart],
            destinations: [this.state.villeArrive],
            travelMode: window.google.maps.TravelMode.DRIVING,
            avoidHighways: false,
            avoidTolls: false
        }, (response, status) => {
            if (status === "OK") {
                let distanceText = response.rows[0].elements[0].distance.text;
                let distanceValue = response.rows[0].elements[0].distance.value;
                let price = this.getPrice(this.state.typeVehicule, this.state.typeTrajet, distanceValue, this.state.nbrHeure, this.state.nbrJour)
                this.setState({ prixTotal: price, distance: distanceText });
                let data = { distance: distanceText, prix: price }
                this._onAddRequest(index, id_user)
                return data;
            } else {
                alert("Error: " + status);
            }
        });
    }
    getPrice = (vehicleType, typeOfTravel, distance, nbHour, nbDay) => {
        if (typeOfTravel === "Aller simple") {
            return this.getOneWayPrice(distance, vehicleType);
        }

        if (typeOfTravel === "Aller-Retour") {
            return this.getRoundTripPrice(distance, vehicleType);
        }

        if (typeOfTravel === "Circuit sur plusieurs heures") {
            return this.getRentalPerHourPrice(nbHour, vehicleType);
        }

        if (typeOfTravel === "Circuit sur plusieurs jours") {
            return this.getRentalPerDayPrice(nbDay, vehicleType);
        }
    }
    getOneWayPrice = (distance, vehicleType) => {
        let price;
        if (vehicleType === "Berline 3 places") {
            price = 1.8 * distance;
            if (price < 55) {
                return 55;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            price = 1.8 * distance;
            if (price < 65) {
                return 65;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            price = 1.8 * distance;
            if (price < 170) {
                return 170;
            }
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            price = 1.8 * distance;
            if (price < 190) {
                return 190;
            }
            return price;
        }
    }
    getRoundTripPrice = (distance, vehicleType) => {
        let price;
        if (vehicleType === "Berline 3 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 55) {
                return 55 * 2;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 65) {
                return 65 * 2;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 170) {
                return 170 * 2;
            }
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            price = 1.8 * distance;
            price = price * 2;
            if (price < 190) {
                return 190 * 2;
            }
            return price;
        }
    }
    getRentalPerHourPrice = (nbHour, vehicleType) => {
        let pricePerHour;
        let price;
        if (vehicleType === "Berline 3 places") {
            pricePerHour = 65;
            price = pricePerHour * nbHour;
            if (price <= 2225) {
                return 250;
            }
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            pricePerHour = 70;
            price = pricePerHour * nbHour;
            if (price <= 255) {
                return 250;
            }
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            pricePerHour = 90;
            price = pricePerHour * nbHour;
            if (price <= 500) {
                return 500;
            }
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            pricePerHour = 90;
            price = pricePerHour * nbHour;
            if (price <= 600) {
                return 600;
            }
            return price;
        }
    }
    getRentalPerDayPrice = (nbDay, vehicleType) => {
        let pricePerDay;
        let price;
        let accommodation = 130;
        if (vehicleType === "Berline 3 places") {
            pricePerDay = 590;
            price = pricePerDay * nbDay;
            if (price <= 590) {
                return 720;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Minivan 6-8 places") {
            pricePerDay = 590;
            price = pricePerDay * nbDay;
            if (price <= 590) {
                return 720;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Minibus 17 places") {
            pricePerDay = 1490;
            price = pricePerDay * nbDay;
            if (price <= 1490) {
                return 990;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }

        if (vehicleType === "Autocar 53-80 places") {
            pricePerDay = 1490;
            price = pricePerDay * nbDay;
            if (price <= 1490) {
                return 990;
            }
            accommodation = accommodation * nbDay;
            price = price + accommodation;
            return price;
        }
    }
    //#endregion


    // fonction qui permet de verifier un email
    validateEmail = (email) => {
        let pattern = /[a-zA-Z0-9]+[\.]?([a-zA-Z0-9]+)?[\@][a-z]{3,9}[\.][a-z]{2,5}/g;
        let result = pattern.test(email);
        if (result) return true;
        else return false;
    }
    //  fonction qui permet de verifier un phone
    validatePhone = (phone) => {
        if (isNaN(phone)) {
            return false
        } else {
            return phone.match(/\d/g).length === 10;
        }

    }
    //permet de verifier les champs
    onVerifyInput = () => {
        if (this.state.typeTrajet === "") {
            this.setState({ styleInputTypeTrajet: styleError })
        } else {
            this.setState({ styleInputTypeTrajet: styleValid })
        }
        if (this.state.typeVehicule === "") {
            this.setState({ styleInputTypeVehicule: styleError })
        } else {
            this.setState({ styleInputTypeVehicule: styleValid })
        }

        if (this.state.villeDepart === "") {
            this.setState({ styleInputVilleDepart: styleError })
        } else {
            this.setState({ styleInputVilleDepart: styleValid })
        }

        if (this.state.villeArrive === "") {
            this.setState({ styleInputVilleArrive: styleError })
        } else {
            this.setState({ styleInputVilleArrive: styleValid })
        }
        if (this.state.input_name === "") {
            this.setState({ styleInputName: styleError })
        } else {
            this.setState({ styleInputName: styleValid })
        }
        if (this.state.input_email !== "" && this.validateEmail(this.state.input_email)) {
            this.setState({ styleInputEmail: styleValid })
        } else {
            this.setState({ styleInputEmail: styleError })
        }
        if (this.state.input_phone !== "" && this.validatePhone(this.state.input_phone)) {
            this.setState({ styleInputPhone: styleValid })
        } else {
            this.setState({ styleInputPhone: styleError })
        }

        if (this.state.typeTrajet !== "" && this.state.typeVehicule !== ""
            && this.state.villeDepart && this.state.villeArrive && this.state.input_name !== ""
            && this.state.input_email !== "" && this.state.input_phone !== "" && this.validateEmail(this.state.input_email)
            && this.validatePhone(this.state.input_phone)) {
            return true
        } else { return false }

    }
    //fonction qui permet d'afficher les info d'un persone (nom,email,num tel,entreprise)
    onClickNext = () => {
        this.setState({
            showNext: true,
        })
    }


    //Fonction est appelé lorsqu'on clique sur le bouton "Ajouter"
    // Si l'utilisateur n'existe pas dans la DB on l'ajoute
    // Vérifier si l'utilisateur ne dépasse pas 5 demandes il peut en ajouter une autre (ajout demande dans la DB)
    onClickAdd = () => {
        //Récupérer la liste des adresse de etapes en une seule chaine de caractere
        let listesDesEtapes = this.state.addressEtapes;
        if (this.state.listEtapes.length > 0) {
            for (let etape of this.state.listEtapes) {
                listesDesEtapes += ', ' + etape;
            }
        }
        let data = this.state.dataTable
        //Vérifiez si l'utilisateur peut ajouter une demande (le nbr demandes < 5) 
        let index = -1;
        index = data.findIndex(obj => obj.designation === "");
        if (index >= 0) {
            if (this.onVerifyInput()) {
                let desc = this.descriptionService(this.state.typeVehicule, this.state.typeTrajet, this.state.nbr_passenger, this.state.nbr_vehicule, null, this.state.villeDepart, this.state.adresseRamassage, listesDesEtapes, this.state.villeArrive, null, null, this.state.dateDepart, null, this.state.dateFinDeService, this.state.heureDebutService, this.state.heureFinService, this.state.nbrHeure, this.state.nbrJour, this.state.infoComplementaire)
                data[index].designation = desc;
                data[index].quantite = this.state.nbr_vehicule;
                this.setState({ dataTable: data, isFinish: true },
                    () => {
                        this._onAddUser(index);
                    })
            }
        } else {
            alert("Vous avez dépassé le nombre max de ligne !!!");
        }
    }

    //Fonction qui permet d'ajouter un utilisateur dans la DB
    _onAddUser = (index) => {
        firebase.database().ref("users").orderByChild("email").equalTo(this.state.input_email).once("value", snapshot => {
            if (!snapshot.exists()) {
                let ref = firebase.database().ref('users').push({
                    nom: this.state.input_name,
                    email: this.state.input_email,
                    mobile: this.state.input_phone,
                    entreprise: this.state.input_entreprise
                })
                this.id_user = ref.key
            } else {
                this.id_user = Object.keys(snapshot.val())[0]
            }
            this.getDistance(index, this.id_user);
        });

    }
    //Fonction qui permet d'ajouter une demande dans la DB
    _onAddRequest = (index, id_user) => {
        let listesDesEtapes = this.state.addressEtapes;
        if (this.state.listEtapes.length > 0) {
            for (let etape of this.state.listEtapes) {
                listesDesEtapes += ', ' + etape;
            }
        }
        let ref = firebase.database().ref('requests/').push({
            typeVehicule: this.state.typeVehicule,
            typeTrajet: this.state.typeTrajet,
            nbrDePassagers: this.state.nbr_passenger,
            nbrDeVehicule: this.state.nbr_vehicule,
            villeDepart: this.state.villeDepart,
            villeArrive: this.state.villeArrive,
            adresseRamassage: this.state.adresseRamassage,
            adresseDestination:this.state.adresseDestination,
            adresseDesEtapes: listesDesEtapes,
            dateDepart: this.state.dateDepart,
            dateFinDeService: this.state.dateFinDeService,
            heureDebutService: this.state.heureDebutService,
            heureFinService: this.state.heureFinService,
            nbrHeure: this.state.nbrHeure,
            nbrJour: this.state.nbrJour,
            infoComplementaire: this.state.infoComplementaire,
            prix: this.state.prixTotal,
            distance: this.state.distance,
            id_user: id_user
        })
        let data = this.state.dataTable
        data[index].key = ref.key
        this.setState({ dataTable: data })
    }

    //fonction est appelé lorsqu'on clic on le bouton Terminer
    onClickFinish = () => {
        window.location.replace('http://sosam.fr/remerciement')
    }

    //Fonction qui permet d'afficher le Formulaire(name,email,phone,...)
    _renderNext = () => {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-12 col-md-6"> <InputText type="text" placeholder="Nom" value={this.state.input_name} handleChange={this.handleChangeName} style={this.state.styleInputName} /></div>
                    <div className="col-xs-12 col-md-6"> <InputText type="text" placeholder="Email" value={this.state.input_email} handleChange={this.handleChangeEmail} style={this.state.styleInputEmail} /></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-md-6">  <InputText type="text" placeholder="Numéro de téléphone" value={this.state.input_phone} handleChange={this.handleChangePhone} style={this.state.styleInputPhone} /></div>
                    <div className="col-xs-12 col-md-6">  <InputText type="text" placeholder="Entreprise" value={this.state.input_entreprise} handleChange={this.handleChangeEntreprise} /></div>
                </div>
                <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: 32 }}>
                    <div className="col-xs-12 col-md-3">
                        <button className='bottomNext' type="submit" onClick={this.onClickAdd}>
                            <span className="textNext">Ajouter</span>
                        </button>
                    </div>
                    {this.state.isFinish ?
                        <div className="col-xs-12 col-md-3">
                            <button className='bottomNext' type="submit" onClick={this.onClickFinish}>
                                <span className="textNext">Terminer</span>
                            </button>
                        </div> : null
                    }
                </div>
            </div>
        )
    }


    //fonction qui permet de mettre à jour le state input_name
    handleChangeName = (event) => {
        let style = styleError;
        if (event.target.value !== "") style = styleValid
        this.setState({ input_name: event.target.value, styleInputName: style });
    }
    //fonction qui permet de mettre à jour le state input_nemail
    handleChangeEmail = (event) => {
        let style = styleError;
        if (event.target.value !== "") style = styleValid
        this.setState({ input_email: event.target.value, styleInputEmail: style });
    }
    //fonction qui permet de mettre à jour le state input_phone
    handleChangePhone = (event) => {
        let style = styleError;
        if (event.target.value !== "") style = styleValid
        this.setState({ input_phone: event.target.value, styleInputPhone: style });
    }
    //fonction qui permet de mettre à jour le state input_adresse
    handleChangeEntreprise = (event) => {
        this.setState({ input_adresse: event.target.value });
    }
    //fonction qui permet de mettre à jour le state input_entreprise
    handleChangeEntreprise = (event) => {
        this.setState({ input_entreprise: event.target.value });
    }

    //fonction qui permet de mettre à jour le state infoComplementaire
    handleChangeInfos_Comp = (event) => {
        this.setState({ infoComplementaire: event.target.value });
    }
    //fonction qui permet de mettre à jour le state nbrHeure
    handleChangeNbrHours = (e) => {
        this.setState({ nbrHeure: e.target.value })
    };
    //fonction qui permet de mettre à jour le state nbrJour
    handleChangeNbrDays = (e) => {
        this.setState({ nbrJour: e.target.value })
    }
    //fonction qui permet de mettre à jour le state heureDebutService
    handleChangeTimeStart = (e) => {
        this.setState({ heureDebutService: e.target.value })
    };
    //fonction qui permet de mettre à jour le state heureFinService
    handleChangeTimeEnd = (e) => {
        this.setState({ heureFinService: e.target.value })
    }

    //#region getDescription
    //fonction qui permet d'afficher la Description
    descriptionService = (typeVehicule, typeTrajet, nbrDePassagers, nbrDeVehicule, nbrDeConducteur, villeDepart, adresseRamassage, etape, villeArrivee, adresseDestination, rayon, dateDepart, heureDepart, dateFinDeService, heureDebutService, heureFinService, nbrHeure, nbrJour, informationsComplementaire) => {
        var description = this.leQuoi(typeVehicule, typeTrajet);
        if ((nbrDePassagers) || (nbrDeVehicule) || (nbrDeConducteur)) {
            description = description + this.pourQui(nbrDePassagers, nbrDeVehicule, nbrDeConducteur);
        }

        if ((villeDepart) || (adresseRamassage) || (etape) || (villeArrivee) || (adresseDestination) || (rayon)) {
            description = description + this.leOu(villeDepart, adresseRamassage, etape, villeArrivee, adresseDestination, rayon);
        }

        if ((dateDepart) || (heureDepart) || (dateFinDeService) || (heureDebutService) || (heureFinService) || (nbrHeure) || (nbrJour)) {
            description = description + this.leQuand(dateDepart, heureDepart, dateFinDeService, heureDebutService, heureFinService, nbrHeure, nbrJour);
        }

        if ((informationsComplementaire)) {
            description = description + this.leComment(informationsComplementaire);
        }
        return description;
    }
    leQuoi = (typeVehicule, typeTrajet) => {
        let quoi;
        let locationDe;
        let pourUn;
        locationDe = "Location " + typeVehicule;
        pourUn = "pour un " + typeTrajet;
        quoi = locationDe + " " + pourUn;
        return quoi;
    }
    pourQui = (nbrDePassagers, nbrDeVehicule, nbrDeConducteur) => {
        let qui = "";
        if (nbrDePassagers) {
            nbrDePassagers = " - Nbr de passagers : " + nbrDePassagers;
            qui = qui + nbrDePassagers;
        }

        if (nbrDeVehicule) {
            nbrDeVehicule = " - Nbr de véhicule(s) : " + nbrDeVehicule;
            qui = qui + nbrDeVehicule;
        }

        if (nbrDeConducteur) {
            nbrDeConducteur = " - Nbr de conducteur(s) : " + nbrDeConducteur;
            qui = qui + nbrDeConducteur;
        }
        return qui;
    }
    leOu = (villeDepart, adresseRamassage, etape, villeArrivee, adresseDestination, rayon) => {
        let ou = "";
        if (villeDepart) {
            villeDepart = " - La ville de départ est " + villeDepart;
            ou = ou + villeDepart;
        }

        if (adresseRamassage) {
            adresseRamassage = " - " + adresseRamassage;
            ou = ou + adresseRamassage;
        }

        if (etape) {
            etape = " - etape(s) durant le trajet : " + etape;
            ou = ou + etape;
        }

        if (villeArrivee) {
            villeArrivee = " - la ville d'arrivée est " + villeArrivee;
            ou = ou + villeArrivee;
        }

        if (adresseDestination) {
            adresseDestination = " - " + adresseDestination;
            ou = ou + adresseDestination;
        }

        if (rayon) {
            rayon = " - dans un rayon de : " + rayon + " km";
            ou = ou + rayon;
        }
        return ou;

    }
    leQuand = (dateDepart, heureDepart, dateFinDeService, heureDebutService, heureFinService, nbrHeure, nbrJour) => {
        let quand = "";
        if (dateDepart) {
            dateDepart = " - Le : " + dateDepart;
            quand = quand + dateDepart;
        }
        if (heureDepart) {
            heureDepart = " à " + heureDepart;
            quand = quand + heureDepart;
        }

        if (dateFinDeService) {
            dateFinDeService = " au : " + dateFinDeService;
            quand = quand + dateFinDeService;
        }

        if (heureDebutService) {
            heureDebutService = " de " + heureDebutService;
            quand = quand + heureDebutService;
        }

        if (heureFinService) {
            heureFinService = " à " + heureFinService;
            quand = quand + heureFinService;
        }

        if (nbrHeure) {
            nbrHeure = " - nbr d'heure(s) : " + nbrHeure;
            quand = quand + nbrHeure;
        }

        if (nbrJour) {
            nbrJour = " nbr de jour(s) : " + nbrJour;
            quand = quand + nbrJour;
        }

        return quand;
    }
    leComment = (informationsComplementaire) => {
        let leComment = "";
        if (informationsComplementaire) {
            informationsComplementaire = " - Détail complémentaire : " + informationsComplementaire;
            leComment = leComment + informationsComplementaire;
        }
        return leComment;
    }
    //# end region 

    //fonction qui permet de supprmer un trajet
    onCancelDesignation = (index, key) => {
        console.log("delete key: " + key)
        firebase.database().ref(`requests/${key}`).remove()
        let data = this.state.dataTable
        data.splice(index, 1);
        data.push({ key: '', designation: '', quantite: '' })
        this.setState({
            dataTable: data
        })

    }
    // choisir le type de vehicule lors on click on vehicule image
    onClickVehicule = (type) => {
        let styleImgWithborder = { width: 100, height: 50, borderWidth: 1, borderStyle: "Solid", borderColor: "#4799df" }
        let styleImgWithoutborder = { width: 100, height: 50}
        if (type === 'autocar') {
            this.setState({
                typeVehicule: 'Autocar 53-80 places',
                isAutocar: true,
                isMinibus: false,
                isMinivan: false,
                isBerline: false,
                styleVehiculeAutocar:styleImgWithborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder
            })
        } else if (type === 'minibus') {
            this.setState({
                typeVehicule: 'Minibus 17 places',
                isMinibus: true,
                isAutocar: false,
                isMinivan: false,
                isBerline: false,
                styleVehiculeMinibus:styleImgWithborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder
            })
        } else if (type === 'minivan') {
            this.setState({
                typeVehicule: 'Minivan 6-8 places',
                isMinivan: true,
                isAutocar: false,
                isMinibus: false,
                isBerline: false,
                styleVehiculeMinivan:styleImgWithborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeBerline:styleImgWithoutborder
            })
        } else {
            this.setState({
                typeVehicule: 'Berline 3 places',
                isBerline: true,
                isAutocar: false,
                isMinibus: false,
                isMinivan: false,
                styleVehiculeBerline:styleImgWithborder,
                styleVehiculeMinibus:styleImgWithoutborder,
                styleVehiculeAutocar:styleImgWithoutborder,
                styleVehiculeMinivan:styleImgWithoutborder
            })
        }
    }

    _renderForm = () => {
        return (
            <div style={{ margin: 16 }}>
                {/*****Type Trajet********/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Type de trajet</div>
                    <div className="col-xs-12 col-md-8">
                        <div onClick={this.showDropdownTypeTrajet}>
                            <input className="inputzone" placeholder="Select..." type="text" value={this.state.typeTrajet} style={this.state.styleInputTypeTrajet} />
                            <img className="arrow" src={this.state.displayMenuTypeTrajet ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                        </div>
                        {
                            this.state.displayMenuTypeTrajet ? (
                                <div className="menuList">
                                    <h1 className="h1-select" onClick={() => this.onClickHandlerTypeTrajet("")}>---</h1>
                                    {
                                        datas_type_Trajet.map((item) =>
                                            (
                                                this.state.typeTrajet === item) ?
                                                <li style={{ listStyleType: "disc", color: "#4799df" }}><span>{item}</span></li>
                                                : <li onClick={() => this.onClickHandlerTypeTrajet(item)}><span>{item}</span></li>
                                        )
                                    }
                                </div>)
                                :
                                null
                        }
                    </div>
                </div>
                {
                    this.state.typeTrajet === "Circuit sur plusieurs heures" ?
                        (
                            <div className="row">
                                <div className="col-xs-12 col-md-4 subTitle">Le nombre d'heures</div>
                                <div className="col-xs-12 col-md-8">
                                    <input className='inputText' type="number" min="0" value={this.state.nbrHeure} onChange={this.handleChangeNbrHours}></input>
                                </div>
                            </div>
                        ) :
                        (
                            this.state.typeTrajet === "Circuit sur plusieurs jours" ?
                                <div className="row">
                                    <div className="col-xs-12 col-md-4 subTitle">Le nombre de jours</div>
                                    <div className="col-xs-12 col-md-8">
                                        <input className='inputText' type="number" min="0" value={this.state.nbrJour} onChange={this.handleChangeNbrDays}></input>
                                    </div>
                                </div>
                                : null
                        )
                }

                {/*****Type Vehicule********/}
                <div className="row">
                    <div className="col-xs-12 col-md-3 imageVehicule"><img style={this.state.styleVehiculeAutocar} src={require("../images/autocar-53-places.png")} onClick={() => this.onClickVehicule('autocar')} className={this.state.isAutocar ? 'selected' : null} /></div>
                    <div className="col-xs-12 col-md-3 imageVehicule"><img style={this.state.styleVehiculeMinibus} src={require("../images/minibus.png")} onClick={() => this.onClickVehicule('minibus')} className={this.state.isMinibus ? 'selected' : null} /></div>
                    <div className="col-xs-12 col-md-3 imageVehicule"><img style={this.state.styleVehiculeMinivan} src={require("../images/minivan.png")} onClick={() => this.onClickVehicule('minivan')} className={this.state.isMinivan ? 'selected' : null} /></div>
                    <div className="col-xs-12 col-md-3 imageVehicule"><img style={this.state.styleVehiculeBerline} src={require("../images/berline.png")} onClick={() => this.onClickVehicule('berline')} className={this.state.isBerline ? 'selected' : null} /></div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Type de véhicule</div>
                    <div className="col-xs-12 col-md-8">
                        <div onClick={this.showDropdownTypeVehicule}>
                            <input className="inputzone" placeholder="Select..." type="text" value={this.state.typeVehicule} style={this.state.styleInputTypeVehicule} />
                            <img className="arrow" src={this.state.displayMenuTypeVehicule ? require("../images/up-arrow.png") : require("../images/down-arrow.png")} />
                        </div>
                        {this.state.displayMenuTypeVehicule ? (
                            <div className="menuList">
                                <h1 className="h1-select" onClick={() => this.onClickHandlerTypeVehicule("")}>---</h1>
                                {
                                    datas_type_Vehicule.map((item) =>
                                        (
                                            this.state.typeVehicule === item) ?
                                            <li style={{ listStyleType: "disc", color: "#4799df" }}><span>{item}</span></li>
                                            : <li onClick={() => this.onClickHandlerTypeVehicule(item)}><span>{item}</span></li>
                                    )
                                }
                            </div>
                        ) :
                            (
                                null
                            )
                        }
                    </div>
                </div>

                {/*****Nombre de véhicule********/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Le nombre de véhicule</div>
                    <div className="col-xs-12 col-md-8">
                        <div>
                            <input type="range" min="0" max="100" step="1" value={this.state.nbr_vehicule} onChange={this.handleChangeNbrVehicle} />
                            <h1 className="h1-range">{this.state.nbr_vehicule}</h1>
                        </div>
                    </div>
                </div>

                {/***** Nombre de passagers ********/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Le nombre de passager</div>
                    <div className="col-xs-12 col-md-8">
                        <div>
                            <input type="range" min="0" max="100" step="1" value={this.state.nbr_passenger} onChange={this.handleChangeNbrPassenger} />
                            <h1 className="h1-range">{this.state.nbr_passenger}</h1>
                        </div>
                    </div>
                </div>

                {/*****Ville Départ********/}
                {this._renderAutoCompleteDepart()}

                {/*Ville arrivée*/}
                {this._renderAutoCompleteArrivee()}

                {/*Address Ramassage*/}
                {this._renderAutoCompleteRamassage()}

                {/*Address Destination*/}
                {this._renderAutoCompleteDestination()}

                {/*Adress des Etapes*/}
                {this._renderAutoCompleteAdrEtapes()}

                {this.state.listEtapes.length > 0 ? this._renderAutoCompleteStep() : null}

                {/*>Date de départ de l'évènement*/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Date de départ</div>
                    <div className="col-xs-12 col-md-8" onClick={this.showDateDepart}>
                        <InputText type="text" placeholder="Date de départ " value={this.state.dateDepart} />
                        {this.state.isShowDateDepart ?
                            <Calendar
                                onChange={(date) => this.onChange(date)}
                                value={this.state.date}
                                nextLabel="Next"
                                next2Label=""
                                prevLabel="Prev"
                                prev2Label=""
                            /> :
                            null
                        }
                    </div>
                </div>

                {/*>Date de fin de l'évènement*/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Date de fin de l'évènement</div>
                    <div className="col-xs-12 col-md-8" onClick={this.showDateFin}>
                        <InputText type="text" placeholder="Date de fin de l'évènement" value={this.state.dateFinDeService} />
                        {this.state.isShowDateFin ?
                            <Calendar
                                onChange={(date) => this.onChangeDateFin(date)}
                                value={this.state.dateFin}
                                nextLabel="Next"
                                next2Label=""
                                prevLabel="Prev"
                                prev2Label=""
                            /> : null}
                    </div>
                </div>

                {/*Heure début de service*/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Heure début de service</div>
                    <div className="col-xs-12 col-md-8">
                        <input type="time" className='inputText' value={this.state.heureDebutService} onChange={this.handleChangeTimeStart}></input>
                    </div>
                </div>

                {/*Heure fin de service*/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle" >Heure fin de service</div>
                    <div className="col-xs-12 col-md-8">
                        <input type="time" className='inputText' value={this.state.heureFinService} onChange={this.handleChangeTimeEnd}></input>
                    </div>
                </div>

                {/*Infos complémentaires*/}
                <div className="row">
                    <div className="col-xs-12 col-md-4 subTitle">Infos complémentaires</div>
                    <div className="col-xs-12 col-md-8">
                        <textarea placeholder="Message" className="textareaInput" value={this.state.infoComplementaire} onChange={this.handleChangeInfos_Comp} />
                    </div>
                </div>

                {/*Button Next*/}
                {
                    this.state.showNext ?
                        <div className="row">
                            <div className="col-xs-12 col-md-12">
                                <button className='bottomTotal'>
                                    <span className="textTotal"> Total</span>
                                </button>
                                <button className='bottomTotalNumber'>
                                    <span className="textTotal">{this.state.prixTotal}</span>
                                </button>
                            </div>
                        </div>
                        :
                        <div className="row">
                            <div className="col-xs-12 col-md-10">
                                <button className='bottomTotal'>
                                    <span className="textTotal"> Total</span>
                                </button>
                                <button className='bottomTotalNumber'>
                                    <span className="textTotal">{this.state.prixTotal}</span>
                                </button>
                            </div>
                            <div className="col-xs-12 col-md-2">
                                <button className='bottomNext' type="submit" onClick={this.onClickNext}>
                                    <span className="textNext"> Next</span>
                                    <img className='iconNext' src={require("../images/next.png")} />
                                </button>
                            </div>
                        </div>
                }
                {
                    this.state.showNext ?
                        this._renderNext() : null
                }

            </div >
        )
    }
    _renderBody = () => {
        return (
            <div style={{ margin: 32 }}>
                <div className="container__table">
                    <table>
                        <thead>
                            <tr>
                                <td style={{ width: '5%', backgroundColor: 'white' }}></td>
                                <td className="td_header" style={{ width: '45%' }}>Designation</td>
                                <td className="td_header" style={{ width: '15%' }}>Véhicule</td>
                                <td className="td_header" style={{ width: '10%' }}>Qté</td>
                                <td className="td_header" style={{ width: '10%' }}>Prix unitaire</td>
                                <td className="td_header" style={{ width: '15%' }}>Montant</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.dataTable.map((item, i) => (
                                <tr style={{ height: 60 }}>
                                    <td className="td_body" style={{ width: '5%', backgroundColor: 'white' }} >{item.designation !== "" ? <img src={require("../images/cancel.png")} onClick={() => this.onCancelDesignation(i, item.key)} /> : null}</td>
                                    <td className="td_body" style={{ width: '45%', backgroundColor: i % 2 === 0 ? '#e1f1ff' : '#f5f5f5' }}>{item.designation}</td>
                                    <td className="td_body" style={{ width: '15%', backgroundColor: 'white' }}>{item.designation !== "" ? <img style={{ width: 80, height: 50 }} src={require("../images/autocar-53-places.png")}></img> : null}</td>
                                    <td className="td_body" style={{ width: '10%', backgroundColor: i % 2 === 0 ? '#e1f1ff' : '#f5f5f5' }}><input className="inputTable" type="text" value={item.quantite} /></td>
                                    <td className="td_body" style={{ width: '10%', backgroundColor: i % 2 === 0 ? '#e1f1ff' : '#f5f5f5' }}><input className="inputTable" type="text" value={this.state.prixUnitaire} /></td>
                                    <td className="td_body" style={{ width: '15%', backgroundColor: i % 2 === 0 ? '#e1f1ff' : '#f5f5f5' }}><input className="inputTable" type="text" value={this.state.montant} /></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '70%', backgroundColor: 'white' }}><label className="label-black">Sous réserve de disponibilité à la date de confirmation</label></div>
                    <div style={{ width: '25%', backgroundColor: '#f5f5f5' }}><input className="inputTable" type="text" /></div>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '70%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '10%', backgroundColor: '#e1f1ff' }}><label className="label-black">HT</label></div>
                    <div style={{ width: '15%', backgroundColor: '#e1f1ff' }}><input className="inputTable" type="text" /></div>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '70%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '10%', backgroundColor: '#f5f5f5' }}><label className="label-black">TVA</label></div>
                    <div style={{ width: '15%', backgroundColor: '#f5f5f5' }}><input className="inputTable" type="text" /></div>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '60%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '17.5%', backgroundColor: '#77acda' }}><label className="label-budget center">BUDGET TOTAL TIC EN EURO</label></div>
                    <div style={{ width: '17.5%', backgroundColor: '#e1f1ff' }}><input className="inputTable" type="text" /></div>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '25%', backgroundColor: '77acda' }}><label className="label center">Modalité et délai de règlement</label></div>
                    <div style={{ width: '70%', backgroundColor: '#f5f5f5' }}><label className="label-black">Versement d’un acompte de 50% à l’acceptation du présent devis<br />Versement du solde une semaine, avant la prestation pour les véhicules. Versement du solde un mois, avant la prestation pour les
                     autocars <br />Vous pouvez, si vous le souhaitez, effectuer un règlement anticipé du "Total à régler » <br />Ce devis ne deviendra commande qu’après réception de votre confirmation écrite, accompagnée de votre acompte de 50% du montant
                     total, sous réserve de nos disponibilités au moment de votre commande.</label></div>
                </div>
                <div className="container__item">
                    <div style={{ width: '5%', backgroundColor: 'white' }}></div>
                    <div style={{ width: '25%', backgroundColor: '77acda' }}><label className="label center"> Mode de règlement</label></div>
                    <div style={{ width: '70%', backgroundColor: '#e1f1ff' }}><label className="label-black">Virement bancaire:<br />IBAN: FR7610278060020002047970169<br />Chèque : A l’ordre de Sosam assos
                    Espèce</label></div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <Row>
                <Col md="6">
                    {this._renderForm()}
                </Col>
                <Col md="6">
                    <Header
                        nom={this.state.input_name}
                        email={this.state.input_email}
                        numTel={this.state.input_phone} />
                    {this._renderBody()}
                    <Footer />
                </Col>
            </Row>
        )
    }
}
export default Main