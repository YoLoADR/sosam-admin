import firebase from "firebase";
const config = {
  apiKey: "AIzaSyA8wx12GqsSqweUxUlwvtC6HkGoe9nYgM0",
  authDomain: "sosam-admin.firebaseapp.com",
  databaseURL: "https://sosam-admin.firebaseio.com/",
  projectId: "sosam-admin",
  storageBucket: "sosam-admin.appspot.com",
  messagingSenderId: "214626501012",
  //appId: "1:214626501012:web:ab6ba7dad1692eccbc8f81"
};
// Initialize Firebase
firebase.initializeApp(config);

export default firebase;