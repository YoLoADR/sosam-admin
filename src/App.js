import React from 'react';
import './App.css';
import Main from './screens/Main';
import FooterMain from './components/FooterMain';
import HeaderMain from './components/HeaderMain';

function App() {
  return (
    <div>
      <HeaderMain />
      <Main />
      <FooterMain />
    </div>
  );
}

export default App;