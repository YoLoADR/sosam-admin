import React, { Component } from 'react';

class HeaderMain extends Component {

    render() {
        return (
            <div className="row" style={{margin:16}}>
                <div className="col-xs-12 col-md-4">
                    <div className="row" >
                        <div className="col-xs-12 col-md-1">
                            <img src={require("../images/multiple-users.png")} /> </div>
                        <div className="col-xs-12 col-md-10">
                            <h6 >SERVICE CLIENT 7J/7</h6>
                            <h6 style={{ fontSize: 12 }}>Une équipe totalement disponible pour vous. Un interlocuteur dédier vous guide du début à la fin et des conducteurs professionnels.</h6>
                        </div>
                    </div>
                </div>



                <div className="col-xs-12 col-md-4">
                    <div className="row" >
                        <div className="col-xs-12 col-md-1">
                            <img src={require("../images/interogation-icon.png")} /> </div>
                        <div className="col-xs-12 col-md-10">
                            <h6 >POSEZ VOS QUESTIONS</h6>
                            <h6 style={{ fontSize: 12 }}>S’il vous manque une info importante qui conditionne votre choix, vous pouvez poser une question à un expert. Vous pourrez ainsi réserver ou non en fonction de nos réponses..</h6>
                        </div>
                    </div>
                </div>


                <div className="col-xs-12 col-md-4">
                    <div className="row" >
                        <div className="col-xs-12 col-md-1">
                            <img src={require("../images/padlock.png")} /> </div>
                        <div className="col-xs-12 col-md-10">
                            <h6 >E-MAIL PROTÉGÉ</h6>
                            <h6 style={{ fontSize: 12 }}>Peur des SPAM ? Nous protégeons votre adresse email grâce à la messagerie interne de SoSam. Quand vous nous adressez une demande, elle n’est communiquer à aucun partenaire et reste dans le cadre d’une demande en ligne.</h6>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}
export default HeaderMain;